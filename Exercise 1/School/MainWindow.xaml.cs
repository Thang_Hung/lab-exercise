﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using School.Data;
using System.Globalization;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;

namespace School
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        // Connection to the School database
        private SchoolDBEntities schoolContext = new SchoolDBEntities();

        // Field for tracking the currently selected teacher
        private Teacher teacher = null;

        // List for tracking the students assigned to the teacher's class
        private IList studentsInfo = null;

        #region Predefined code

        public MainWindow()
        {
            InitializeComponent();
        }

        // Connect to the database and display the list of teachers when the window appears
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            teachersList.DataContext = schoolContext.Teachers;
        }


        // When the user selects a different teacher, fetch and display the students for that teacher
        private void teachersList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Find the teacher that has been selected
            this.teacher = teachersList.SelectedItem as Teacher;
            this.schoolContext.LoadProperty<Teacher>(this.teacher, s => s.Students);

            // Find the students for this teacher
            this.studentsInfo = ((IListSource)teacher.Students).GetList();


            // Use databinding to display these students
            studentsList.DataContext = this.studentsInfo;
        }


        #endregion

        // When the user presses a key, determine whether to add a new student to a class, remove a student from a class, or modify the details of a student
        private void studentsList_KeyDown(object sender, KeyEventArgs e)
        {
            //Create a new obj 
            Student student = null;

            //object declaration studentForm
            StudentForm form1;

            //Check format Datetime dd/mm/yyyy
            Regex rgx = new Regex(@"^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$");

            switch (e.Key)
            {
                //If the user pressed Enter, edit the details for the currently selected student
                case Key.Enter:
                    //get student data from the Listview
                    student = studentsList.SelectedItem as Student;

                    //Create a new Form
                    form1 = new StudentForm();

                    //convert the data of student into the textbox value of the StudentForm form
                    form1.Title = "Edit Detail Student";
                    form1.firstName.Text = student.FirstName;
                    form1.lastName.Text = student.LastName;
                    form1.dateOfBirth.Text = student.DateOfBirth.ToString("dd/MM/yyyy");

                    //convert the textbox value of studentForm into data of Student
                    if (form1.ShowDialog().Value)
                    {
                        //Check student values ​​must not be empty
                        if (form1.firstName.Text.Trim() != "" && form1.lastName.Text.Trim() != "" && form1.dateOfBirth.Text.Trim() != "")
                        {
                            //Checks for a valid datetime value
                            if (rgx.IsMatch(form1.dateOfBirth.Text.Trim()))
                            {
                                student.FirstName = form1.firstName.Text.Trim();
                                student.LastName = form1.lastName.Text.Trim();
                                student.DateOfBirth = DateTime.Parse(form1.dateOfBirth.Text.Trim()).Date;

                                //Enable button SaveChange
                                saveChanges.IsEnabled = true;
                            }
                            else
                                MessageBox.Show("Invalid datetime value");
                        }
                        else
                            MessageBox.Show("Student values ​​cannot be empty");
                        
                    }

                    break;
                //If the user pressed Insert, Create new data for student
                case Key.Insert:

                    //Create a new Form
                    form1 = new StudentForm();
                    form1.Title = "Create a new Student";

                    //convert the textbox value of studentForm into data of Student
                    if (form1.ShowDialog().Value)
                    {
                        //Check student values ​​must not be empty
                        if (form1.firstName.Text.Trim() != "" && form1.lastName.Text.Trim() != "" && form1.dateOfBirth.Text.Trim() != "")
                        {
                            //Checks for a valid datetime value
                            if (rgx.IsMatch(form1.dateOfBirth.Text.Trim()))
                            { 
                                //Create Random primarykey
                                Random ran = new Random();
                                int Id = ran.Next();

                                //Check Primarykey
                                while (schoolContext.Students.Where(x => x.Id == Id).SingleOrDefault() != null)
                                    Id = ran.Next();

                                //Create new Student
                                student = new Student();

                                //student.Id = Id;
                                student.FirstName = form1.firstName.Text.Trim();
                                student.LastName = form1.lastName.Text.Trim();
                                student.DateOfBirth = DateTime.Parse(form1.dateOfBirth.Text.Trim()).Date;
                                student.ClassId = (teachersList.SelectedItem as Teacher).Id;

                                //Add student data in database
                                schoolContext.Students.AddObject(student);

                                //Enable button SaveChange
                                saveChanges.IsEnabled = true;
                            }
                            else
                                MessageBox.Show("Invalid datetime value");
                        }
                        else
                        {
                            MessageBox.Show("Student values ​​cannot be empty");
                        }
                    }

                    break;
                //If the user pressed Delete, delete student data for the currently selected student
                case Key.Delete:

                    //Create a Form Prompt the user to confirm that they want to remove the selected student from the class 
                    var Result = MessageBox.Show("Are you sure you want to delete the student that is currently selected ?", "Delete student data", MessageBoxButton.YesNo, MessageBoxImage.Question);

                    if (Result == MessageBoxResult.Yes)
                    {
                        //get student data from the Listview
                        student = studentsList.SelectedItem as Student;

                        //Delete Student data in listview
                        schoolContext.Students.DeleteObject(student);

                        //Enable button SaveChange
                        saveChanges.IsEnabled = true;
                    }

                    break;
                default:
                    break;
            }
        }

        #region Predefined code

        private void studentsList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        // Save changes back to the database and make them permanent
        private void saveChanges_Click(object sender, RoutedEventArgs e)
        {
            //Save Data in database
            schoolContext.SaveChanges();

            //Disable button SaveChange
            saveChanges.IsEnabled = false;

            MessageBox.Show("Update Data successful");
        }

        #endregion
    }

    [ValueConversion(typeof(string), typeof(Decimal))]
    class AgeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
                              System.Globalization.CultureInfo culture)
        {

            int age = DateTime.Now.Year - DateTime.Parse(value.ToString()).Year;
            return age.ToString();

        }

        #region Predefined code

        public object ConvertBack(object value, Type targetType, object parameter,
                                  System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
