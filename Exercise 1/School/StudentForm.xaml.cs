﻿using School.Data;
using System;
using System.Windows;

namespace School
{
    /// <summary>
    /// Interaction logic for StudentForm.xaml
    /// </summary>
    public partial class StudentForm : Window
    {
        
        #region Predefined codeS

        // TODO: Exercise 1: Task 2b: Set the title of the form and populate the fields on the form with the details of the student
        // TODO: Exercise 1: Task 3a: Display the form
        public StudentForm()
        {
            InitializeComponent();
        
        }
    
        
        private void ok_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        #endregion

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }
    }
}
